<?php

/**
 * @file
 * Worker functions for pathauto_regen.module.
 */

/**
 * Batch callback that updates a node's path given a mlid.
 */
function pathauto_regen_regenerate($mlid, array &$context) {
  $link_path = db_select('menu_links', 'ml')
    ->fields('ml', array('link_path'))
    ->condition('ml.mlid', $mlid)
    ->execute()
    ->fetchField();

  $nid = substr($link_path, 5);

  if (!$node = node_load($nid)) {
    return;
  }

  $node->path = array();
  if ($path = path_load('node/' . $node->nid)) {
    $node->path = $path;
  }

  pathauto_node_update_alias($node, 'update');

  if (empty($context['results']['processed'])) {
    $context['results']['processed'] = 0;
  }

  $context['results']['processed']++;
  $context['message'] = t('Updating the path alias for %title.', array('%title' => $node->title));
}

/**
 * Callback for batch completion.
 *
 * @todo Handle error cases. I haven't run into any yet. :)
 */
function pathauto_regen_finished($success, array $results, array $operations, $time) {
  if ($success) {
    $message = format_plural($results['processed'],
      'Updated %count node in %time.',
      'Updated %count nodes in %time.',
      array('%count' => $results['processed'], '%time' => $time)
    );
    drupal_set_message($message);
  }
  else {
    // Handle error case.
  }
}
